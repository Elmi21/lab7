#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#define FILE_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
#define CONST_FORMAT 3
#define CONST_FIGHTER 5

//возвращает число цифр в числе - 1 или 2. Предполагается, что число участников не может превышать 99

int number_characters(int n)
{

    if (n / 10 == 0)
        return 1;
    else return 2;
}
//запись в cтроку числа бойцов

void write_to_line(char *str_out, char *str_in, int num_fighter, int j)
{
    sprintf(str_in, "%d", num_fighter);
    if (number_characters(num_fighter) == 1) {
        str_out[j] = '0';
        str_out[j + 1] = str_in[0];
    } else {
        str_out[j] = str_in[0];
        str_out[j + 1] = str_in[1];
    }
    str_out[j + 2] = '|';
}

void parent(int readfd, int writefd, char *str, int size_str, int i, int *stat, int num_teams)
{
    int num_fighter;
    write(writefd, str, size_str);
    read(readfd, str, size_str);
    str[size_str - 1] = '\0';
    printf("Родитель считал информацию из канала о количестве участников от потомка №%d\n", i);
    printf("%s\n", str);
    for (int j = 0; j < num_teams * CONST_FORMAT; j = j + CONST_FORMAT) {
        str[0] = str[j];
        str[1] = str[j + 1];
        num_fighter = atoi(str);
        if (num_fighter == 0) *stat = 1;
    }
}

void child(int readfd, int writefd, int size_str, int num_teams, int i)
{
    int flag = 0, num_fighter, k, rand_num;
    char str_in[size_str], str_out[size_str], str[2];
    for (;;) {
        read(readfd, str_in, size_str);
        str_in[size_str - 1] = '\0';
        k = 0;
        for (int j = 0; j < num_teams * CONST_FORMAT; j = j + CONST_FORMAT) {
            str[0] = str_in[j];
            str[1] = str_in[j + 1];
            num_fighter = atoi(str);
            if (num_fighter == 0)
                return;
            if (i != k) {
                rand_num = 1 + rand() % num_fighter;
                num_fighter = num_fighter - rand_num;
                if (num_fighter == 0)
                    flag = 1;
                write_to_line(str_out, str, num_fighter, j);
            } else {
                rand_num = 1 + rand() % CONST_FIGHTER;
                num_fighter = num_fighter + rand_num;
                sprintf(str, "%d", num_fighter);
                write_to_line(str_out, str, num_fighter, j);
            }
            k++;
        }
        str_out[CONST_FORMAT * num_teams] = '\0';
        write(writefd, str_out, size_str);
        if (flag == 1)
            return;
    }
}

void main()
{

    int num_teams, size_str, stat, flag_finish = 0;
    printf("Введите количество команд\n");
    scanf("%d", &num_teams);
    pid_t PidsProcess[num_teams];
    char name_channel[2], name_channel_w[2], name_channel_r[2];
    size_str = num_teams * CONST_FORMAT + 1;
    char str[size_str];
    int readfd[num_teams], writefd[num_teams];
    srand(time(NULL));
    //создание каналов
    for (int i = 0; i < num_teams * 2; i++) {

        sprintf(name_channel, "%d", i);
        if ((mkfifo(name_channel, 0666) < 0) && (errno != EEXIST)) {
            printf("Канал не создан %s!\n", name_channel);
            exit(1);
        } else {
            printf("Канал  создан %s!\n", name_channel);
        }
    }
    //формирование строки с информацией о начальном количестве участников
    for (int i = 0; i < num_teams * CONST_FORMAT; i = i + CONST_FORMAT) {
        str[i] = '1';
        str[i + 1] = '0';
        str[i + 2] = '|';
    }
    str[num_teams * CONST_FORMAT] = '\0';
    //процессы-потомки
    for (int i = 0; i < num_teams; i++) {
        PidsProcess[i] = fork();
        if (-1 == PidsProcess[i]) {
            perror("Ошибка!\n");
            exit(1);
        } else if (0 == PidsProcess[i]) {
            sprintf(name_channel_r, "%d", i);
            readfd[i] = open(name_channel_r, O_RDONLY, 0);
            sprintf(name_channel_w, "%d", i + num_teams);
            writefd[i] = open(name_channel_w, O_WRONLY, 0);
            child(readfd[i], writefd[i], size_str, num_teams, i);
            exit(0);
        }
    }

    //открытие каналов родителем
    for (int i = 0; i < num_teams; i++) {
        sprintf(name_channel_w, "%d", i);
        writefd[i] = open(name_channel_w, O_WRONLY, 0);
        sprintf(name_channel_r, "%d", i + num_teams);
        readfd[i] = open(name_channel_r, O_RDONLY, 0);
    }
    printf("Изначально в командах было %s участников\n", str);
    for (;;) {
        for (int i = 0; i < num_teams; i++) {
            parent(readfd[i], writefd[i], str, size_str, i, &stat, num_teams);
            if (stat == 1) {
                for (int i = 0; i < num_teams; i++)
                    kill(PidsProcess[i], SIGKILL);
                flag_finish = 1;
                break;
            }
        }
        if (flag_finish == 1)
            break;
    }
    for (int i = 0; i < num_teams; i++) {
        close(readfd[i]);
        close(writefd[i]);
    }
    for (int i = 0; i < num_teams * 2; i++) {

        sprintf(name_channel, "%d", i);
        unlink(name_channel);
    }
    printf("Выполнение программы закончено!\n");
}
